import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import {PlayerModel} from "./model/player.model";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript


admin.initializeApp({
 credential: admin.credential.cert(cert),
 databaseURL: "https://les-fauconniers.firebaseio.com"
});

const db = admin.firestore();
const playersRef = db.collection('players');

async function addPlyers(newPlayer: PlayerModel): Promise<PlayerModel> {
 if(!newPlayer) {
  throw new Error('newPlyer does not exist')
 }
 const addPlyer = await playersRef.add(newPlayer);
 return ({...newPlayer, id: addPlyer.id})
}

 export const on_post_player = functions
     .region("europe-west1")
     .runWith({memory: "256MB"})
     .https
     .onRequest(async (request, response) => {
      const newPlayer = request.body;
      const addResult = await addPlyers(newPlayer);
  response.send(addResult);
 });
