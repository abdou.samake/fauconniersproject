export interface PlayerModel {
    firstName?: string;
    lastName?: string;
    registration?: string;
    email?: string;
    id?: string;
}
