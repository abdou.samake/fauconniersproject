import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import {DocumentSnapshot} from "firebase-functions/lib/providers/firestore";
import {PlayerModel} from "./model/player.model";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript

admin.initializeApp({
 credential: admin.credential.cert(cert),
 databaseURL: "https://les-fauconniers.firebaseio.com"
});
const db = admin.firestore();
const playersRef = db.collection('players');

export async function testPlayerExistById(playerId: string) {
 const playerRef = playersRef.doc(playerId);
 const snapShotPlayerRef: DocumentSnapshot = await playerRef.get();
 const playerToFind: PlayerModel = snapShotPlayerRef.data() as PlayerModel;
 if(!playerToFind) {
  throw new Error('player does note exist');
 }
 return playerRef;
}


export async function deletePlayerById(playerId: string): Promise<string> {
 const playerToDelete = await testPlayerExistById(playerId);
 await playerToDelete.delete();
 return ` player id: ${playerToDelete.id} is delete `;
}

 export const do_delete_player = functions
     .region("europe-west1")
     .runWith({memory: "256MB"})
     .https
     .onRequest(async (request, response) => {
      const result = await deletePlayerById("qAo5BmrFZkm6n8II4CfD");

  response.send(result);
 });
