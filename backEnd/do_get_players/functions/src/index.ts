import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import {PlayerModel} from "./model/player.model";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://les-fauconniers.firebaseio.com"
});

const db = admin.firestore();

export const do_get_players = functions
    .region("europe-west1")
    .runWith({memory: "256MB"})
    .https
    .onRequest(async (request, response) => {
        const playersRef = db.collection('players');
        const snapShotPlayersRef = await playersRef.get();
        const players: PlayerModel[] = [];
        snapShotPlayersRef.forEach((snapPlayer) => players.push(snapPlayer.data()));
  response.send(players);
 });
