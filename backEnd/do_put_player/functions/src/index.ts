import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {cert} from "./cred/cert";
import {DocumentSnapshot} from "firebase-functions/lib/providers/firestore";
import {PlayerModel} from "./model/player.model";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript

admin.initializeApp({
 credential: admin.credential.cert(cert),
 databaseURL: "https://les-fauconniers.firebaseio.com"
});

const db = admin.firestore();
const playersRef = db.collection('players');

export async function testPlayerExistById(playerId: string) {
 const playerRef = playersRef.doc(playerId);
 const snapShotPlayerRef: DocumentSnapshot = await playerRef.get();
 const playerToFind: PlayerModel = snapShotPlayerRef.data() as PlayerModel;
 if(!playerToFind) {
  throw new Error('player does note exist');
 }
 return playerRef;
}

export async function putPlayerById(playerId: string, newPlayer: PlayerModel): Promise<any>{
 if(!playerId || !newPlayer) {
  throw new Error('playerId or newPlayer are not exists');
 }
 const playerToPut = await testPlayerExistById(playerId);
 return playerToPut.set(newPlayer);
}
 export const do_put_player = functions
     .region("europe-west1")
     .runWith({memory: "256MB"})
     .https
     .onRequest(async (request, response) => {
      const newPlayer = request.body;
      const result = await putPlayerById("8m2vOPq4xK2bZ7kANEFU", newPlayer);
  response.send(result);
 });
